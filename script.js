var btn;
var drop;
var dropveille;
var more;
var veilles;
var titlenew;
var authornew;
var descnew;
var sourcenew;
var linknew;
var submit;
var change;
var cancel;


$(document).ready(function() {
  $('#btn1').click(function() {
    $.ajax({
      type:"GET",
      url: "http://veille.popschool.fr/api/?api=veille&action=getAll",
      complete: function(r) {
        var array = JSON.parse(r.responseText).veilles.length;
        // $("#id_input").html(JSON.parse(r.responseText).veilles[array-1].id);
        // $("#id_user").html(JSON.parse(r.responseText).veilles[array-1].id_user);
        $("#title").html(JSON.parse(r.responseText).veilles[array-1].title);
        $("#description").html(JSON.parse(r.responseText).veilles[array-1].description);
        $("#author").html(JSON.parse(r.responseText).veilles[array-1].firstname);
        $("#source").html(JSON.parse(r.responseText).veilles[array-1].source);
        $("#link").html(JSON.parse(r.responseText).veilles[array-1].link);
      }
    });
    update();
  });

  more = function() {
    btn.style.display = "none";
    drop.style.display="block";

    $.ajax({
       type:"GET",
       url: "http://veille.popschool.fr/api/?api=veille&action=getAll",
       complete: function(r) {
         var array = JSON.parse(r.responseText).veilles.length;
         veilles = JSON.parse(r.responseText).veilles;
         for(var i = 0; i < array; i++) {
           dropveille.innerHTML+="<li class='text-center' onclick='genStamp(" + i + "),update()' id='button"+i+"''>" +
           trunc(veilles[i].title) +
           "</li><li role='separator' class='divider'></li><br>";
             }
         }
       });

   };

   btn = document.getElementById("btn");
   drop = document.getElementById("drop");
   dropveille = document.getElementById("dropveille");
   id_input = document.getElementById("id_input");
   id_user = document.getElementById("id_user_input");
   date = document.getElementById("date_input");
   type = document.getElementById("type_input");
   titlenew = document.getElementById("titlenew");
   authornew = document.getElementById("authornew");
   descnew = document.getElementById("descnew");
   sourcenew = document.getElementById("sourcenew");
   linknew = document.getElementById("linknew");
   submit = document.getElementById("submit");
   change = document.getElementById("change");
   cancel = document.getElementById("cancel");


   drop.style.display="none";
   titlenew.style.display="none";
   authornew.style.display="none";
   descnew.style.display="none";
   sourcenew.style.display="none";
   linknew.style.display="none";
   submit.style.display="none";
   change.style.display="none";
   cancel.style.display="none";

 });

  modify = function(){
    titlenew.style.display="block";
    authornew.style.display="block";
    descnew.style.display="block";
    sourcenew.style.display="block";
    linknew.style.display="block";
    submit.style.display="block";
    $("#cancel").show();
  };
  update = function(){
    change.style.display="block";
  };

  function cancellulite() {
    titlenew.style.display="none";
    authornew.style.display="none";
    descnew.style.display="none";
    sourcenew.style.display="none";
    linknew.style.display="none";
    submit.style.display="none";
    $("#cancel").hide();
  };

  genStamp = function(i) {
    document.getElementById("id_input").value=veilles[i].id;
    document.getElementById("date_input").value=veilles[i].date;
    document.getElementById("id_user_input").value=veilles[i].id_user;
    document.getElementById("type_input").value=veilles[i].type;
    $("#title").html(veilles[i].title);
    $("#description").html(veilles[i].description);
    $("#author").html(veilles[i].firstname);
    $("#source").html(veilles[i].source);
    $("#link").html(veilles[i].link);
    console.log(veilles[i]);
  };

  function trunc(elem) {
    if(elem.length > 13) {
      return elem.substr(0,13).concat('...');
    } else {
      return elem
    }
  }



 //Enregistrement de la veille contenu dans le formulaire
 function save() {
   var f = new Object();
   f.id = document.getElementById("id_input").value;
   f.id_user = document.getElementById("id_user_input").value;
   f.date = document.getElementById("date_input").value;
   f.title = document.getElementById("titlenew").value;
   f.description = document.getElementById("descnew").value;
   f.link = document.getElementById("linknew").value;
   f.type = document.getElementById("type_input").value;
   f.source = document.getElementById("sourcenew").value;
   f.firstname = document.getElementById("authornew").value;
     console.log(f);

     //Demande les infos de l'utilisateur
     email_user = prompt("e-mail : ","");
     password_user = prompt("password : ","");

     //Envois des données
     $.ajax({
       url: "http://veille.popschool.fr/api/",
       data: {api: "veille", action:"save", email: email_user, password: password_user, veille:JSON.stringify(f)},
       complete: function(r) {
         console.log(r.responseText);
         console.log(f);
       }
     });
   };
